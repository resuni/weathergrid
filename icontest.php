<!doctype html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Icons Test</title>

    <!-- https://erikflowers.github.io/weather-icons/ -->
    <link
     rel="stylesheet"
     href="css/weather-icons.min.css"
    >

    <style>
      i.wIcon {
        font-size: 50pt;
        padding: 20px;
      }
    </style>
  </head>
  <body>

<?php
require('class.php');

$o = new Cell("2018-12-23 03:00:00", 800, "clear sky", 261.51);

echo "    <h2>Day</h2>\n";

foreach ($o->icons_day as $wID => $icon) {
  print("      $wID <i $icon></i><br />\n");
}

echo "    <h2>Night</h2>\n";

foreach ($o->icons_night as $wID => $icon) {
  print("      $wID <i $icon></i><br />\n");
}

?>

  </body>
</html>
