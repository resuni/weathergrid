<?php

class Cell {

  public $content;

  // Icon definitions.
  // This array associates OWM's weather descriptions to a WeatherIcons string.
  // https://openweathermap.org/weather-conditions
  public $icons_day = [
    // Group 2xx: Thunderstorm
    200 => "class='wi wi-thunderstorm wIcon'",
    201 => "class='wi wi-thunderstorm wIcon'",
    202 => "class='wi wi-thunderstorm wIcon'",
    210 => "class='wi wi-thunderstorm wIcon'",
    211 => "class='wi wi-thunderstorm wIcon'",
    212 => "class='wi wi-thunderstorm wIcon'",
    221 => "class='wi wi-thunderstorm wIcon'",
    230 => "class='wi wi-thunderstorm wIcon'",
    231 => "class='wi wi-thunderstorm wIcon'",
    232 => "class='wi wi-thunderstorm wIcon'",
    // Group 3xx: Drizzle
    300 => "class='wi wi-rain wIcon'",
    301 => "class='wi wi-rain wIcon'",
    302 => "class='wi wi-rain wIcon'",
    310 => "class='wi wi-rain wIcon'",
    311 => "class='wi wi-rain wIcon'",
    312 => "class='wi wi-rain wIcon'",
    313 => "class='wi wi-rain wIcon'",
    314 => "class='wi wi-rain wIcon'",
    321 => "class='wi wi-rain wIcon'",
    // Group 5xx: Rain
    500 => "class='wi wi-day-rain wIcon'",
    501 => "class='wi wi-day-rain wIcon'",
    502 => "class='wi wi-day-rain wIcon'",
    503 => "class='wi wi-day-rain wIcon'",
    504 => "class='wi wi-day-rain wIcon'",
    511 => "class='wi wi-snowflake-cold wIcon'",
    520 => "class='wi wi-rain wIcon'",
    521 => "class='wi wi-rain wIcon'",
    522 => "class='wi wi-rain wIcon'",
    531 => "class='wi wi-rain wIcon'",
    // Group 6xx: Snow
    600 => "class='wi wi-snowflake-cold wIcon'",
    601 => "class='wi wi-snowflake-cold wIcon'",
    602 => "class='wi wi-snowflake-cold wIcon'",
    611 => "class='wi wi-snowflake-cold wIcon'",
    612 => "class='wi wi-snowflake-cold wIcon'",
    615 => "class='wi wi-snowflake-cold wIcon'",
    616 => "class='wi wi-snowflake-cold wIcon'",
    620 => "class='wi wi-snowflake-cold wIcon'",
    621 => "class='wi wi-snowflake-cold wIcon'",
    622 => "class='wi wi-snowflake-cold wIcon'",
    // Group 7xx: Atmosphere
    701 => "class='wi wi-fog wIcon'",
    711 => "class='wi wi-fog wIcon'",
    721 => "class='wi wi-fog wIcon'",
    731 => "class='wi wi-fog wIcon'",
    741 => "class='wi wi-fog wIcon'",
    751 => "class='wi wi-fog wIcon'",
    761 => "class='wi wi-fog wIcon'",
    762 => "class='wi wi-fog wIcon'",
    771 => "class='wi wi-fog wIcon'",
    781 => "class='wi wi-fog wIcon'",
    // Group 800: Clear
    800 => "class='wi wi-day-sunny wIcon'",
    // Group 80x: Clouds
    801 => "class='wi wi-day-cloudy wIcon'",
    802 => "class='wi wi-day-cloudy wIcon'",
    803 => "class='wi wi-day-cloudy wIcon'",
    804 => "class='wi wi-day-cloudy wIcon'",
  ];

  public $icons_night = [
    // Group 2xx: Thunderstorm
    200 => "class='wi wi-thunderstorm wIcon'",
    201 => "class='wi wi-thunderstorm wIcon'",
    202 => "class='wi wi-thunderstorm wIcon'",
    210 => "class='wi wi-thunderstorm wIcon'",
    211 => "class='wi wi-thunderstorm wIcon'",
    212 => "class='wi wi-thunderstorm wIcon'",
    221 => "class='wi wi-thunderstorm wIcon'",
    230 => "class='wi wi-thunderstorm wIcon'",
    231 => "class='wi wi-thunderstorm wIcon'",
    232 => "class='wi wi-thunderstorm wIcon'",
    // Group 3xx: Drizzle
    300 => "class='wi wi-rain wIcon'",
    301 => "class='wi wi-rain wIcon'",
    302 => "class='wi wi-rain wIcon'",
    310 => "class='wi wi-rain wIcon'",
    311 => "class='wi wi-rain wIcon'",
    312 => "class='wi wi-rain wIcon'",
    313 => "class='wi wi-rain wIcon'",
    314 => "class='wi wi-rain wIcon'",
    321 => "class='wi wi-rain wIcon'",
    // Group 5xx: Rain
    500 => "class='wi wi-night-alt-rain wIcon'",
    501 => "class='wi wi-night-alt-rain wIcon'",
    502 => "class='wi wi-night-alt-rain wIcon'",
    503 => "class='wi wi-night-alt-rain wIcon'",
    504 => "class='wi wi-night-alt-rain wIcon'",
    511 => "class='wi wi-snowflake-cold wIcon'",
    520 => "class='wi wi-rain wIcon'",
    521 => "class='wi wi-rain wIcon'",
    522 => "class='wi wi-rain wIcon'",
    531 => "class='wi wi-rain wIcon'",
    // Group 6xx: Snow
    600 => "class='wi wi-snowflake-cold wIcon'",
    601 => "class='wi wi-snowflake-cold wIcon'",
    602 => "class='wi wi-snowflake-cold wIcon'",
    611 => "class='wi wi-snowflake-cold wIcon'",
    612 => "class='wi wi-snowflake-cold wIcon'",
    615 => "class='wi wi-snowflake-cold wIcon'",
    616 => "class='wi wi-snowflake-cold wIcon'",
    620 => "class='wi wi-snowflake-cold wIcon'",
    621 => "class='wi wi-snowflake-cold wIcon'",
    622 => "class='wi wi-snowflake-cold wIcon'",
    // Group 7xx: Atmosphere
    701 => "class='wi wi-fog wIcon'",
    711 => "class='wi wi-fog wIcon'",
    721 => "class='wi wi-fog wIcon'",
    731 => "class='wi wi-fog wIcon'",
    741 => "class='wi wi-fog wIcon'",
    751 => "class='wi wi-fog wIcon'",
    761 => "class='wi wi-fog wIcon'",
    762 => "class='wi wi-fog wIcon'",
    771 => "class='wi wi-fog wIcon'",
    781 => "class='wi wi-fog wIcon'",
    // Group 800: Clear
    800 => "class='wi wi-night-clear wIcon'",
    // Group 80x: Clouds
    801 => "class='wi wi-night-alt-cloudy wIcon'",
    802 => "class='wi wi-night-alt-cloudy wIcon'",
    803 => "class='wi wi-night-alt-cloudy wIcon'",
    804 => "class='wi wi-night-alt-cloudy wIcon'",
  ];

  // Constructor
  // * $date = Date in text format, NOT epoch.
  // * $wID = Weather ID.
  // * $wDesc = Weather description.
  // * $temp = Temperature.
  function __construct ($date, $wID, $wDesc, $temp) {

    // Get hour from $date.
    $hour = $date->format('H');

    // Determine if it's day or night based on the hour (06 through 18 is day).
    if ($hour > 5 and $hour < 18) {
      $icon = $this->icons_day;
    } else {
      $icon = $this->icons_night;
    }

    // Build cell.
    $this->content = "        <td>" .
     "<i " . $icon[$wID] . " title='" . $wDesc . "'></i>" .
     "<br />" .
     $this->t_convert($temp) . "&deg; F" .
     "</td>\n";
  }

  // Function to convert Kelvin to Fahrenheit
  function t_convert (float $k) {
    $f = ($k - 273.15) * 1.8 + 32;
    $f = round($f); 
    return $f;
  }

}

?>
