<?php 
  // Get initial PHP declarations out of the way.

  // File where all my classes are dumped.
  require("class.php");

  // Get weather data from data.json.
  $data = json_decode(file_get_contents("data.json"));

  // Use America/Denver time zone when displaying dates/times.
  $dTZ = new DateTimeZone('America/Denver');

  // Create object for current date/time.
  $now = new DateTime();
  $now->setTimezone($dTZ);

  // Create DateTime object for desired forecast time limit.
  $limit = $now;
  $limit = $limit->add(new DateInterval("PT12H"));

?>
<!doctype html>
<html lang="en" xml:lang="en">
  <head>
 
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
 
    <title>Weather</title>
 
    <!-- Bootstrap -->
    <link 
     rel="stylesheet"
     href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
     integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
     crossorigin="anonymous"
    >

    <!-- https://erikflowers.github.io/weather-icons/ -->
    <link
     rel="stylesheet"
     href="css/weather-icons.min.css"
    >

    <style>

     body {
       margin:1in;
     }

     table {
       text-align: center;
     }

     th.cityName {
       text-align: left;
     }

     i.wIcon {
       font-size: 50pt;
     }

    </style>
  </head>
  <body>
    <table class="table table-bordered">
      <tr>
        <th></th>
<?php

  // Create column for current weather data.
  $dt = new DateTime("@" . reset($data)[0]->dt);
  $dt->setTimezone($dTZ);
  echo '        <th>' . $dt->format('D H:i') . "</th>\n";

  // Loop through list data in first city and create columns for forecast data.
  foreach (reset($data)[1]->list as $target) {

    $dt = new DateTime("@$target->dt");

    // Convert to time zone specified at the top of the file.
    $dt->setTimezone($dTZ);

    // Only create table columns for today's data.
    if ($dt < $limit) {
      // Create column for this time.
      echo '        <th>' . $dt->format('D H:i') . "</th>\n";
    } else {
      break;
    }
  }
?>
      </tr>
<?php
  // Loop through JSON data as each city.
  foreach ($data as $cityName => $cityData) {

    echo <<<EOT
      <tr>
        <th class=cityName>$cityName</th>\n
EOT;

    // If the response code is not 200, display an error message.
    if ($cityData[1]->cod != 200) {
      echo "        <td colspan=100% class=fail>$cityData[1]->message</td>\n";
    } else {

      // Current weather's time.
      $dt = new DateTime("@" . $cityData[0]->dt);
      $dt->setTimezone($dTZ);
      // Build cell for current weather information.
      $cell = new Cell(
        $dt,
        $cityData[0]->weather[0]->id,
        $cityData[0]->weather[0]->description,
        $cityData[0]->main->temp
      );
      echo $cell->content;

      // Loop through list data of each city.
      foreach ($cityData[1]->list as $target) {
        // Keep track of time.
        $dt = new DateTime("@$target->dt");
        $dt->setTimezone($dTZ);

        // Loop through today's weather forecast.
        if ($dt < $limit) {
          $cell = new Cell(
            $dt,
            $target->weather[0]->id,
            $target->weather[0]->description,
            $target->main->temp
          );
          echo $cell->content;
        }
      }
    }

  }
?>
    </table>
  </body>
</html>
