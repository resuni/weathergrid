<?php

require("APPID.php");

$locations = array(
  "Colorado Springs"    => 5417598,
  "Monument"            => 5431740,
  "Perry Park"          => 5434370,
  "Castle Rock"         => 5416329,
  "Castle Pines"        => 5416322,
  "Lone Tree"           => 5429208,
  "Centennial"          => 5416541,
  "Denver"              => 5419384,
  "Westminster"         => 5443910,
  "Broomfield"          => 5415035,
  "Superior"            => 5440838,
  "Boulder"             => 5574991,
);

function performcurl (string $url) {

  $curlopts = array(
    CURLOPT_URL => $url,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_POST => false,
  );

  $ci = curl_init();
  curl_setopt_array($ci, $curlopts);
  $response = curl_exec($ci);
  curl_close($ci);

  return $response;
}

$data = array();

foreach ($locations as $key => $l) {

  $data[$key] = [];

  // Get current weather data for this iteration's location.
  $currentData = performcurl(
   "https://api.openweathermap.org/data/2.5/weather" .
   "?APPID=" . $appid .
   "&id=" . $l
  );
  array_push($data[$key], json_decode($currentData));
  sleep(1);

  // Get weather forecast for this iteration's location.
  $forecastData = performcurl(
   "http://api.openweathermap.org/data/2.5/forecast" .
   "?APPID=" . $appid .
   "&id=" . $l
  );
  array_push($data[$key], json_decode($forecastData));
  sleep(1);

}

file_put_contents("data.json.new", json_encode($data));
rename("data.json.new", "data.json");

?>
